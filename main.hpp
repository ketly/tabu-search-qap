/* Autora: Kétly Gonçalves Machado
 * Dezembro/2017
 * Tópicos Especiais em Redes de Computadores II
 * Curso de Ciência da Computação
 * Universidade Federal da Fronteira Sul
 * Professor: Claunir Pavan
 *
 * Sobre: Este trabalho tem o objetivo de implementar uma metaheurística
 * para resolução do QAP - Quadratic Assignment Problem. A metaheurística
 * utilizada foi a Busca Tabu.
 */

#include <bits/stdc++.h>

using namespace std;

#define pb push_back
#define sz(v) ((int)v.size())
#define rad(a) a * PI / 180

#define MAXN 123 // Número máximo de nodos na rede
#define MAXR 500 // Número máximo de iterações sem solução melhorada na Busca Tabu
#define MAXC 1 << 16 // Número máximo de soluções diferentes suportado pelo controle de ciclos
#define MINT 2 // Tamanho mínimo da Lista Tabu
const int MAXI = 1 << 15; // Número máximo de iterações da Busca Tabu

typedef long long ll;
typedef vector <int> vu;

const double PI = 3.14159265358979;
const ll INF = 0x3F3F3F3F3F3F3F3FLL; // Constante alusiva a um valor infinito
const ll fatormultiplicador = 100000000; // Fator usado para trabalhar apenas com inteiros

struct nodo {
  string nome;
  int id;
  long double lt, lg;
  nodo(string _nome, int _id, long double _lt, long double _lg) : nome(_nome), id(_id), lt(_lt), lg(_lg) {}
};

struct par {
  int i, j;
  par() {}
  par(int _i, int _j) : i(_i), j(_j) {}

  // Sobrescreve o operador '<'
  bool operator <(const par &_p) const {
    return (i == _p.i ? j < _p.j : i < _p.i);
  }

  // Sobrescreve o operador '>'
  bool operator >(const par &_p) const {
    return (i == _p.i ? j > _p.j : i > _p.i);
  }

  // Sobrescreve o operador '=='
  bool operator ==(const par &_p) const {
    return (i == _p.i && j == _p.j);
  }
};

extern int n_nodos; // Quantidade de nodos na rede
extern int MF[MAXN][MAXN]; // Matriz de fluxos
extern ll MD[MAXN][MAXN]; // Matriz de distâncias
extern vector <nodo> N; // Vetor que armazena os dados sobre os nodos da rede

struct solucao {

  vu ps; // Representa uma permutação da topologia que é solução para o problema, não necessariamente ótima
  ll val_FO; // Valor da função objetivo aplicada à permutação

  solucao() {}
  solucao(vu _ps, ll _val_FO) : ps(_ps), val_FO(_val_FO) {}

  // Sobrescreve o operador '='
  void operator =(solucao &_s) {
    val_FO = _s.val_FO;
    ps = _s.ps;
  }

  // Sobrescreve o operador '<'
  bool operator <(const solucao &_s) const {
    return (val_FO < _s.val_FO);
  }

  // Sobrescreve o operador '>'
  bool operator >(const solucao &_s) const {
    return (val_FO > _s.val_FO);
  }

  // Sobrescreve o operador '=='
  bool operator ==(const solucao &_s) const {
    return (val_FO == _s.val_FO);
  }

  // Inverte os valores das posições u e v da permutação e atualiza o valor da função objetivo
  void _swap(int u, int v) {

    int i;

    for (i = 0; i < n_nodos; i++) {
      if (i == u || i == v) continue;
      if (MF[u][i] == 1)
        val_FO -= MD[ps[u]][ps[i]];
      if (MF[v][i] == 1)
        val_FO -= MD[ps[v]][ps[i]];
    }
    if (MF[u][v] == 1)
      val_FO -= MD[ps[u]][ps[v]];

    swap(ps[u], ps[v]);

    for (i = 0; i < n_nodos; i++) {
      if (i == u || i == v) continue;
      if (MF[u][i] == 1)
        val_FO += MD[ps[u]][ps[i]];
      if (MF[v][i] == 1)
        val_FO += MD[ps[v]][ps[i]];
    }
    if (MF[u][v] == 1)
      val_FO += MD[ps[u]][ps[v]];
  }

  void print() {
    printf("\nCusto Total:  %Lf\n\n", ((long double)val_FO / (long double)fatormultiplicador));
    printf("Atribuição da topologia:");
    for (int i = 0; i < sz(ps); i++) {
      if (i % 4 == 0) printf("\nphi(%2d) = %2d'", i, ps[i]);
      else {
        printf(" phi(%2d) = %2d'", i, ps[i]);
      }
    }
    printf("\n");
  }
};

struct listaTabu {

  set <par> T; // Representa a Lista Tabu e é usado para buscar de forma eficiente se um movimento é tabu
  queue <par> Q; // Representa a Lista Tabu e é usado para armazenar e remover os movimentos tabu

  // Retorna o movimento tabu mais antigo
  par maisAntigo() {
    return Q.front();
  }

  // Remove um movimento da Lista Tabu
  void remove() {
    par u = Q.front();
    T.erase(u);
    Q.pop();
  }

  // Adiciona um movimento à Lista Tabu
  void adiciona(par x, int tam) {
    if (sz(Q) == tam) remove();
    T.insert(x);
    Q.push(x);
  }

  // Consulta se um movimento é tabu
  bool ehTabu(par x) {
    return T.find(x) != T.end();
  }

  // Redimensiona a Lista Tabu
  void redimensiona(int tam) {
    while (sz(Q) > tam) {
      remove();
    }
  }

  // Reseta as estruturas de armazenamento da Lista Tabu
  void reseta() {
    T.clear();
    while (!Q.empty()) Q.pop();
  }
};

long double todouble(string a);
int carrega_dados(char *rede);
long double haversine(long double lt1, long double lg1, long double lt2, long double lg2);
void calcula_distancias();
ll funcao_objetivo(vu p);
solucao solucao_inicial();
bool funcaoAspiracao(solucao atual, solucao melhor);
void redimensionaListaTabu(bool aumenta, int &t, listaTabu &LT);
void atualizaAspiracao(solucao s, solucao x, map <ll, int> &controleCiclos, int &tamLT, int &i_ssn, listaTabu &LT);
solucao busca_tabu();
int main(int argc, char *argv[]);
