/* Autora: Kétly Gonçalves Machado
 * Dezembro/2017
 * Tópicos Especiais em Redes de Computadores II
 * Curso de Ciência da Computação
 * Universidade Federal da Fronteira Sul
 * Professor: Claunir Pavan
 *
 * Sobre: Este trabalho tem o objetivo de implementar uma metaheurística
 * para resolução do QAP - Quadratic Assignment Problem. A metaheurística
 * utilizada foi a Busca Tabu.
 */

#include "main.hpp"

int tamLT; // Define o tamanho da Lista Tabu

/* Usada para melhorar a Busca Tabu, faz com que um movimento tabu seja utilizado
 * se apresentar uma solução melhor do que aquela que se tem como a melhor solução
 * até o momento.
 */
bool funcaoAspiracao(solucao atual, solucao melhor) {
  return atual < melhor;
}

// Modifica o tamanho da Lista Tabu conforme a necessidade
void redimensionaListaTabu(bool aumenta, int &t, listaTabu &LT) {
  if (aumenta) {
    t = t << 1;
    if (t > (n_nodos * n_nodos - n_nodos) / 2) {
      t = (n_nodos * n_nodos - n_nodos) / 2;
    }
  } else {
    t = t >> 1;
    if (t < MINT) {
      t = MINT;
    }
    LT.redimensiona(t);
  }
}

// Auxilia na melhor da busca controlando a existência de um ou mais ciclos
void atualizaAspiracao(solucao s, solucao x, map <ll, int> &controleCiclos, int &tamLT, int &i_ssn, listaTabu &LT) {
  // Se a solução já apareceu alguma vez
  if (controleCiclos.find(s.val_FO) != controleCiclos.end()) {
    controleCiclos[s.val_FO]++;
    i_ssn++;
    if (i_ssn > MAXR) // Se o número de iterações sem solução nova é maior que o máximo, temos um ciclo
      redimensionaListaTabu(1, tamLT, LT); // Aumenta o tamanho da Lista Tabu
    if (controleCiclos[s.val_FO] > MAXR) // Se o número vezes que uma mesma solução foi encontrada é maior que o máximo, temos um ciclo
      redimensionaListaTabu(1, tamLT, LT); // Aumenta o tamanho da Lista Tabu
  } else {
    controleCiclos[s.val_FO] = 1;
    i_ssn = 1;
    // Se o número de soluções diferentes ultrapassar o máximo, reseta o controle de ciclos
    if (sz(controleCiclos) > MAXC) {
      redimensionaListaTabu(0, tamLT, LT);
      controleCiclos.clear();
    }
  }
}

solucao busca_tabu() {

  int i_ssn = 0; // Número de iterações sem solução nova
  int i, Mi; // Variáveis para controle de iterações e número da melhor iteração
  int u, v; // Váriaveis para iteração nos nodos da rede
  int mov; // Flag para controle da realização de um movimento
  par movimento; // Armazena o movimento realizado
  solucao s = solucao_inicial(); // Solução inicial para a busca
  solucao x = solucao_inicial(); // Melhor solução encontrada
  listaTabu LT; // Lista Tabu
  map <ll, int> controleCiclos; // Estrutura para controle de ciclos na busca

  tamLT = MINT;

  for (i = 0, Mi = 0; (i - Mi) < MAXI; i++) {
    solucao minima = s;
    minima.val_FO = INF;

    mov = 0;
    for (u = 0; u < n_nodos; u++) {
      for (v = u + 1; v < n_nodos; v++) {
        solucao aux = s;
        aux._swap(u, v);
        if ((!LT.ehTabu(par(u, v)) && !LT.ehTabu(par(v, u))) || funcaoAspiracao(aux, x)) {
          if (aux < minima) {
            minima = aux;
            movimento = par(u, v);
            mov = 1;
          }
        }
      }
    }

    // Se não houve movimento, retira o movimento tabu mais antigo da lista e o realiza
    if (!mov) {
      movimento = LT.maisAntigo();
      LT.remove();
      minima._swap(movimento.i, movimento.j);
    }

    atualizaAspiracao(minima, x, controleCiclos, tamLT, i_ssn, LT);

    s = minima;
    LT.adiciona(movimento, tamLT);
    swap(movimento.i, movimento.j);
    LT.adiciona(movimento, tamLT);

    // Se a menor solução encontrada na vizinhança de s é melhor do que todas as soluções já encontradas, define esta como a melhor
    if (minima < x) {
      x = minima;
      Mi = i;
      i_ssn = 0;
    }
  }
  return x;
}
