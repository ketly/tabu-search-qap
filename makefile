all: clean compile

compile: headerfile
	g++ -std=c++11 *.cpp -Wall -O2 -o tabu

headerfile:
	g++ -std=c++11 main.hpp -Wall -O2

clean:
	rm -f *.gch *.o
