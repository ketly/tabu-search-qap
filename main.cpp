/* Autora: Kétly Gonçalves Machado
 * Dezembro/2017
 * Tópicos Especiais em Redes de Computadores II
 * Curso de Ciência da Computação
 * Universidade Federal da Fronteira Sul
 * Professor: Claunir Pavan
 *
 * Sobre: Este trabalho tem o objetivo de implementar uma metaheurística
 * para resolução do QAP - Quadratic Assignment Problem. A metaheurística
 * utilizada foi a Busca Tabu.
 */

#include "main.hpp"

int n_nodos; // Quantidade de nodos na rede
int MF[MAXN][MAXN]; // Matriz de fluxos
ll MD[MAXN][MAXN]; // Matriz de distâncias
vector <nodo> N; // Vetor que armazena os dados sobre os nodos da rede

// Converte uma string para um valor real
long double todouble(string a) {

  int pos, i, neg = 0;
  long double r = 0., base;

  if (a[0] == '-') {
    a.erase(0, 1);
    neg = 1;
  }

  pos = a.find('.');
  if (pos == -1) pos = sz(a);
  base = 1;
  for (i = pos - 1; i >= 0; i--) {
    r += (a[i] - '0') * base;
    base *= 10;
  }
  base = 0.1;
  for (i = pos + 1; i < sz(a); i++) {
    r += (a[i] - '0') * base;
    base *= 0.1;
  }
  return (neg ? (r * (-1)) : r);
}

// Carrega as informações sobre a rede dos arquivos "_links" e "_nodes"
int carrega_dados(char *rede) {

  int id = 0, i, j;
  long double lt = 0, lg = 0;
  string linha, nome, nome2, aux;
  string caminho("dados/");
  ifstream arq;
  map <string, int> map_aux;

  caminho += rede;
  caminho += "_nodes.csv";
  arq.open(caminho);
  if (!arq) return 1;

  getline(arq, linha);
  while (getline(arq, linha) && sz(linha) > 1) {
    for (i = 0, j = 0; i < sz(linha); i++) {
      if (linha[i] == ',') {
        if (j == 1) lt = todouble(aux);
        else if (j == 2) lg = todouble(aux);
        j++;
        if (j == 3) break;
        aux.erase(aux.begin(), aux.end());
        continue;
      }
      if (j == 0) nome += linha[i];
      else aux += linha[i];
    }
    N.push_back(nodo(nome, id, lt, lg));
    map_aux[nome] = id;
    nome.erase(nome.begin(), nome.end());
    id++;
  }
  arq.close();

  caminho.erase(caminho.begin() + strlen(rede) + 6, caminho.end());
  caminho += "_links.csv";
  arq.open(caminho);
  if (!arq) return 1;

  getline(arq, linha);
  while (getline(arq, linha) && sz(linha) > 1) {
    for (i = 0, j = 0; i < sz(linha); i++) {
      if (linha[i] == ',') {
        j++; i++;
        if (j == 2) break;
      }
      if (j == 0) nome += linha[i];
      else nome2 += linha[i];
    }
    MF[map_aux[nome]][map_aux[nome2]] = MF[map_aux[nome2]][map_aux[nome]] = 1;
    nome.erase(nome.begin(), nome.end());
    nome2.erase(nome2.begin(), nome2.end());
  }
  arq.close();

  n_nodos = id;

  return 0;
}

// Calcula a fórmula de haversine
long double haversine(long double lt1, long double lg1, long double lt2, long double lg2) {

  long double R = 6373.0;
  long double rlt1 = rad(lt1);
  long double rlt2 = rad(lt2);
  long double rlg1 = rad(lg1);
  long double rlg2 = rad(lg2);
  long double u = sin((rlt2 - rlt1)/2);
  long double v = sin((rlg2 - rlg1)/2);
  long double a, d;

  a = u * u + cos(rlt1) * cos(rlt2) * v * v;
  d = 2.0 * R * atan2(sqrt(a), sqrt(1 - a));

  return d;
}

// Calcula as distâncias entre os nodos da rede
void calcula_distancias() {

  int i, j;

  for (i = 0; i < n_nodos; i++) {
    for (j = i + 1; j < n_nodos; j ++) {
      // Converte os valores double para inteiros, para evitar erros de precisão
      MD[i][j] = MD[j][i] = ((long double)fatormultiplicador * haversine(N[i].lt, N[i].lg, N[j].lt, N[j].lg));
    }
  }
}

// Retorna o valor da função objetivo para a solução inicial
ll funcao_objetivo(vu p) {

  int i, j;
  ll valor = 0;

  for (i = 0; i < sz(p); i++) {
    for (j = i + 1; j < sz(p); j++) {
      if (MF[i][j]) {
        valor += MD[p[i]][p[j]];
      }
    }
  }
  return valor;
}

// Retorna a solução inicial para ser utilizada pela Busca Tabu
solucao solucao_inicial() {

  int i;
  vu permutacao;
  ll valor;

  for (i = 0; i < n_nodos; i++) permutacao.pb(i);
  valor = funcao_objetivo(permutacao);

  return solucao(permutacao, valor);
}

int main(int argc, char *argv[]) {

  char rede[25]; // Nome da rede

  sscanf(argv[1], "%s", rede);

  memset(MF, 0, sizeof(MF));
  if (carrega_dados(rede)) {
    printf("Falha ao abrir arquivos da rede.\n");
    return 1;
  }

  calcula_distancias();

  for (int i = 0; i < n_nodos; i++) {
    for (int j = 0; j < n_nodos; j++) {
      printf("%Lf ", ((long double)MD[i][j] / (long double)fatormultiplicador));
    }
    printf("\n");
  }

  solucao_inicial().print();
  busca_tabu().print();

  return 0;
}
